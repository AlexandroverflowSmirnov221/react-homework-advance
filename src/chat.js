import React, { useEffect } from "react";
import Header from "./components/header/header";
import Main from "./components/main/main";
import Preloader from "./components/preloader/preloader";
import Context from "./context";


function Chat() {
  const [data, setData] = React.useState([]);
  const [preloader, setPreloader] = React.useState(true);
  const [edit, setEdit] = React.useState(null);
  const [ownData, setOwnData] = React.useState([
    {id: Math.random(), createdAt: new Date('2022-01-26T13:51:50'), text:"Hello BSA"}
  ])

  useEffect(() => {
  fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
    .then(response => response.json())
    .then( data => {
      setTimeout(() =>{
      setData(data)
      setPreloader(false)
      }, 1500)
      })
  }, [])


  function removeMessage(id) {
    setOwnData(ownData.filter(ownData => ownData.id !== id))
  }

  function editMessage(id) {
    const findMessage = ownData.find(ownData => ownData.id === id);
    setEdit(findMessage);
  }

  function toggleLike(id) {
    setData(
      data.map(data => {
        if(data.id === id) {
          data.editedAt =!data.editedAt; 
        }
        return data;
      })
    )
  }
  
  function addMessage(text) {
    setOwnData(ownData.concat([{
      text,
      id: Math.random(),
      createdAt: Date.now(),
    }]))
  }


  return (
    <Context.Provider value={{editMessage, removeMessage, addMessage, toggleLike}}>
      <div className='wrapper'>
        {preloader && <Preloader/>}
        {preloader ? null:
        <div className='page-container'>
          <Header data={data} ownData={ownData} key={ownData.id}/>
          <Main data={data} ownData={ownData} edit={edit} setEdit={setEdit}/>
        </div>}
      </div>
    </Context.Provider>
  );
}

export default Chat;
